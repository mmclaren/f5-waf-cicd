#!/bin/bash
set_vars() {
    export DEV_BRANCH="dev"
    if [ "$CI_COMMIT_REF_NAME" == "master" ]
    then
        echo "building master branch, setting staging environment variables"
        export deploy_env="staging"
        export vs_ip="10.0.60.210"
        export backend_server_address="192.168.100.200"
        export waf_policy_name="juiceshop_waf_policy_staging"
        return 0
    else
        echo "building tagged, setting production environment variables"
        export deploy_env="production"
        export vs_ip="10.0.60.210"
        export backend_server_address="192.168.1.200"
        export waf_policy_name="juiceshop_waf_policy_production"
        return 0
    fi
}
check_blocked() {
    if grep -q "Request Rejected" /tmp/resp.$1
    then
        echo "Request was rejected"
        return 0
    else
        echo "Request was not rejected"
        return 1
    fi
}

check_pass() {
    if grep -q "Request Rejected" /tmp/resp.$1
    then
        echo "Request was rejected"
        touch /tmp/suggest_mod
        return 0
    else
        echo "Request accepted"
        return 0
    fi
}

check_suggestions() {
    if [ -f /tmp/suggest_mod ];
    then
        sleep 10
        polid=$(curl -sku admin:{password} https://3.105.159.80/mgmt/tm/asm/policies?\$filter=name%20eq%20"juiceshop_waf_policy_staging"\&\$select=name,id | jq -r '.items[0].id')
        taskid=$(curl -X POST -sku admin:{password} https://3.105.159.80/mgmt/tm/asm/tasks/export-suggestions -d '{ "policyReference": { "link": "https://localhost/mgmt/tm/asm/policies/'${polid}'" }, "inline": true }' | jq -r '.id')
        echo $taskid
        sleep 10
        suggestion=$(curl -sku admin:{password} https://3.105.159.80/mgmt/tm/asm/tasks/export-suggestions/$taskid | jq -r '.result.suggestions')
        modification=$(echo $suggestion | sed 's/^.//' | sed 's/.$//')
        echo "the suggested modifications based on policy builder are:"
        echo $modification
        echo ${GITLAB_IP} gitlab.com >> /etc/hosts
        git config --global user.email "${GITLAB_USER_LOGIN}@f5.com"
        git config --global user.name "${GITLAB_USER_LOGIN}"
        git config --global http.sslVerify false
        git clone http://${GITLAB_USER_LOGIN}:${GITLAB_PASS}@gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}.git
        cd ${CI_PROJECT_NAME}
        git checkout -b ${DEV_BRANCH}
        cp waf_policy.json suggestions/new_waf_policy.json
        sed -i "s/${waf_policy_name}/waf_policy_name/g" waf_policy.json
        new_policy=$(awk -v "a=$modification" '/modifications/{print; print a;next}1' waf_policy.json)
        echo $new_policy > /tmp/new_waf_policy.json
        jq . /tmp/new_waf_policy.json > suggestions/new_waf_policy.json
        echo "new policy.json:"
        cat suggestions/new_waf_policy.json
        git add suggestions/*
        git commit -a -m "add suggestions to the waf policy"
        git push -f origin ${DEV_BRANCH}
        echo "Trusted traffic failed. Check /suggestions in Git repo"
        return 1
    else
        echo "Trusted traffic passed successfully"
        return 0
    fi
}

function upload_file_to_bigip() {
    export size=`stat -c %s $1`
    export size_minus=`expr $size - 1`
    export range="0-${size_minus}/${size}"
    echo $range
    echo $bigip_server
    curl -H "Content-Range: $range" --data-binary "@$1" -sku admin: {password} https://3.105.159.80/mgmt/tm/asm/file-transfer/uploads/policy.json
}

waf_to_bigip() {
    sed -i "s/waf_policy_name/${waf_policy_name}/g" waf_policy.json
    echo "Upload Policy to BIG-IP"
    upload_file_to_bigip waf_policy.json
    echo "Import Policy to BIG-IP"
    curl -sku admin:{password} -X POST https://3.105.159.80//mgmt/tm/asm/tasks/import-policy/ -d'{"filename":"policy.json","policy":{"fullPath":"/Common/'${waf_policy_name}'"}}'
    echo "Apply Policy"
    curl -sku admin:{password} -X POST https://3.105.159.80//mgmt/tm/asm/tasks/apply-policy/ -d'{"policy":{"fullPath":"/Common/'${waf_policy_name}'"}}'
}


trusted_traffic() {
    curl -o /tmp/resp.json -H "X-Forwarded-For: 10.10.10.10" -k "http://$vs_ip/ftp/Packages.json"
    curl -o /tmp/resp.yml -H "X-Forwarded-For: 10.10.10.10" -k "http://$vs_ip/ftp/errors.yml"
    curl -o /tmp/resp.bak -H "X-Forwarded-For: 10.10.10.10" -k "http://$vs_ip/ftp/security_report.json.bak"
}

vulnerability_scan() {
    vs_name=serviceMain
    echo VS Name $vs_name
    vs_ip=`curl -k -u admin:$DOCKERHUB_PASS https://${BIG_IP}/mgmt/tm/ltm/virtual/~staging~juiceshop~$vs_name |jq .destination |cut -d'%' -f 1 |cut -d'/' -f 3 |cut -d':' -f 1`
    echo VS IP $vs_ip
    sleep 5
    curl -o /tmp/resp.xss -k "http://$vs_ip/api/Products/1" -H "Content-Type:application/json" --data-binary '{"description":"<script>alert(\"XSS3\")</script>"}'
    curl -o /tmp/resp.ftp -k "http://$vs_ip/ftp/package.json.bak%2500.md"
    curl -o /tmp/resp.login -k "http://$vs_ip/index.php?username=1'%20or%20'1'%20=%20'1&password=1'%20or%20'1'%20=%20'1"
    curl -o /tmp/resp.pw -k "http://$vs_ip/rest/user/change-password?current=abcde&new=slurmCl4ssic&repeat=slurmCl4ssic"
}

function dast() {
  vs_name=serviceMain
  echo VS Name $vs_name
  vs_ip=`curl -k -u admin:$DOCKERHUB_PASS https://${BIG_IP}/mgmt/tm/ltm/virtual/~production~juiceshopprod~$vs_name |jq .destination |cut -d'%' -f 1 |cut -d'/' -f 3 |cut -d':' -f 1`
  echo VS IP $vs_ip
  export DAST_WEBSITE=http://$vs_ip/
  /analyze -t $DAST_WEBSITE
}

build() {
  echo "Saving USER and ROOM variables as Project-level VARs"
  curl -sH "PRIVATE-TOKEN:${GITLAB_TOKEN}" -X POST "http://${GITLAB_IP}/api/v4/projects/10/variables" --form "key=USER" --form "value=$user"
  curl -sH "PRIVATE-TOKEN:${GITLAB_TOKEN}" -X POST "http://${GITLAB_IP}/api/v4/projects/10/variables" --form "key=ROOM" --form "value=$room"
}

revert_policy() {
        echo ${GITLAB_IP} gitlab.com >> /etc/hosts
        git config --global user.email "${GITLAB_USER}@f5.com"
        git config --global user.name "${GITLAB_USER}"
        git config --global http.sslVerify false
        git clone http://${GITLAB_USER}:${DOCKERHUB_PASS}@gitlab.com/${GITLAB_USER}/${REPO_NAME}.git
        cd ${REPO_NAME}
        git checkout ${DEV_BRANCH}
        jq 'del(.modifications[])' suggestions/new_waf_policy.json > waf_policy.json
        rm -rf suggestions/new_waf_policy.json
        git add *
        git commit -a -m "reverting to original WAF policy"
        git push -f origin ${DEV_BRANCH}
}
