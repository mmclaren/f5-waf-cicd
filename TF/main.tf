provider "aws" {
  profile = "default"
  region  = ap-southeast-2
}

resource "aws_instance" "bigip" {
  ami           = f5_waf
  instance_type = t2.medium
  subnet_id = subnet-0bed001a62a745a36
  key_name = var.MM
  user_data = templatefile("user_data.sh.tpl", { 
     bigip_password = var.bigip_password, 
     bigip_license = var.bigip_license 
  })

  tags = {
    for k, v in merge({
      app_type = "bigip"
      Name = "mm-bigip"
    },
    var.default_ec2_tags): k => v
  }
}

output "bigip_ip" {
  value = aws_instance.bigip.public_ip
}
